library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
entity case2 is
port(
 pSW: in STD_LOGIC_VECTOR( 7 downto 0);
 pLED: out std_logic_vector(7 downto 0);
 pLED1: out std_logic_vector(7 downto 0)  
);
end case2;
architecture c2 of case2 is
begin
process(pSW)
begin
case pSW is
		when "00000001" =>
			pLED1 <= "00000011";
			pLED <= "11001100";
		when "00000010" =>
			pLED1 <= "00000011";
			pLED <= "11000011";
		when "00000000" =>
			pLED1 <= "00000011";
			pLED <= "00001111";
		when "00000011" =>
			pLED1 <= "00000000";
			pLED <= "11110000";
		when "00001010" =>
			pLED1 <= "11001100";
			pLED <= "00000011";
		when others =>
			pLED1 <= "00000000";
			pLED <= "00000000";
	end case;	
	end process;
end c2;
