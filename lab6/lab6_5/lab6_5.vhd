library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
entity case5 is
port( 
	pSW: in std_logic_vector(7 downto 0);
	pHEX0: out std_logic_vector(7 downto 0);
	pHEX1: out std_logic_vector(7 downto 0);
	pHEX2: out std_logic_vector(7 downto 0);
	pHEX3: out std_logic_vector(7 downto 0)
);
end case5;
architecture n of case5 is
begin
	process(pSW) is
	begin 
		if (pSW(0) = '1') then
			pHEX0 <= "11000000"; 
			pHEX1 <= "10100100";
			pHEX2 <= "11000000"; 
			pHEX3 <= "10100100";
		else 
			pHEX0 <= "10010000"; 
			pHEX1 <= "11111001";
			pHEX2 <= "11000000"; 
			pHEX3 <= "10100100";
		end if; 
	end process;
end n;
