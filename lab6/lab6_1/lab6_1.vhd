library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;
entity test is 
port(
 pSW: in std_logic_vector (7 downto 0);
 pLED: out std_logic_vector(7 downto 0);
 pLED1: out std_logic_vector(7 downto 0)
 );
end test;
architecture t1 of test is
begin
 pLED(4) <= transport pSW(2);
 end architecture;