library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
entity case3 is
port( 
	pSW: in std_logic_vector(7 downto 0);
	pHEX0: out std_logic_vector(7 downto 0);
	pHEX1: out std_logic_vector(7 downto 0);
	pHEX2: out std_logic_vector(7 downto 0);
	pHEX3: out std_logic_vector(7 downto 0)
);
end case3;
architecture cs3 of case3 is
begin
	process(pSW) is
	begin 
		if (pSW(0) = '1') then
			pHEX0 <= "11111000"; 
			pHEX1 <= "11111111";
			pHEX2 <= "11111000"; 
			pHEX3 <= "11111111";
		else 
			pHEX0 <= "11111111"; 
			pHEX1 <= "10000010";
			pHEX2 <= "11111111"; 
			pHEX3 <= "10000010";
		end if; 
	end process;
end cs3;
