library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
 
entity case4 is
 
port(
	pSW: in std_logic_vector(7 downto 0);
	pLED: out std_logic_vector(7 downto 0);
	pLED1: out std_logic_vector(7 downto 0);
	clock: in std_logic);
end case4;
 
architecture n of case4 is
	signal t: integer := 0;
	signal state: std_logic := '0';
 
begin
	process is
		begin 
			wait until rising_edge(clock);
				t <= t + 1;
			if t > 3500000 then
				t <= 0;
				state <= not state;
				pLED(4) <= state;
			end if;
	end process;
end architecture;
