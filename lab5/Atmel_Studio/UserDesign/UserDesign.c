// ######################################################################################
// #                                                                                    #
// #  This module implements the users design                                           #
// #                                                                                    #
// ######################################################################################

#include <util/delay.h>
#include "UserDesign.h"

AutomatStates_t State;


// ######################################################################################
// #  This function initializes the finite state machine with start state               #
// ######################################################################################
void StateMachineInit(void)
{
    State = DriveDown;
}

// ######################################################################################
// #  This function updates the current state of the finite state machine               #
// ######################################################################################
void StateMachineUpdate(void)
{
    switch (State)
     {
		 case DriveDown:
		 {
			 Actuators.DriveUpwards               = 0;
			 Actuators.DriveDownwards             = 1;
			 Actuators.DoorFloor2Open             = 0;
			 Actuators.DoorFloor2Close            = 0;
			 Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
			 
			 if (Sensors.ElevatorOnFloor1)
			 State = DriveUp;
			 else if (Sensors.ElevatorAboveFloor2 & Sensors.CallButtonFloor2Up)
			 State = DriveDownToo;
			 
			 break;
		 }
         
         case DriveUp:
         {
	         Actuators.DriveUpwards               = 1;
	         Actuators.DriveDownwards             = 0;
			 Actuators.DoorFloor2Open             = 0;
			 Actuators.DoorFloor2Close            = 0;
			 Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
             
             if (Sensors.ElevatorOnFloor_4)
             State = DriveDown;
			 else if (Sensors.ElevatorBelowFloor2 & Sensors.CallButtonFloor2Up)
			 State = DriveUpToo;
             
             break;
         }
		 
         case DriveUpToo:
         {
	         Actuators.DriveUpwards               = 1;
	         Actuators.DriveDownwards             = 0;
	         Actuators.DoorFloor2Open             = 0;
			 Actuators.DoorFloor2Close            = 0;
	         Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
	         
	         if (Sensors.ElevatorOnFloor2 & Sensors.Floor2DoorClosed)
	         State = Open;
	         
	         break;
         }

         case DriveDownToo:
         {
	         Actuators.DriveUpwards               = 0;
	         Actuators.DriveDownwards             = 1;
	         Actuators.DoorFloor2Open             = 0;
			 Actuators.DoorFloor2Close            = 0;
	         Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
	         
	         if (Sensors.ElevatorOnFloor2 & Sensors.Floor2DoorClosed)
	         State = Open;
	         
	         break;
         }

         case Open:
         {
	         Actuators.DriveUpwards               = 0;
	         Actuators.DriveDownwards             = 0;
	         Actuators.DoorFloor2Open             = 1;
			 Actuators.DoorFloor2Close            = 0;
	         Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
	   
	         if (Sensors.Floor2DoorOpen)
	         State = Close;
			 
	         break;
         }
		 
         case Close:
         {
	         Actuators.DriveUpwards               = 0;
	         Actuators.DriveDownwards             = 0;
	         Actuators.DoorFloor2Open             = 0;
			 Actuators.DoorFloor2Close            = 1;
	         Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
	         
			 if (Sensors.Floor2DoorClosed)
			 State = DriveUp;
			 
	         break;
         }
         
     }
}